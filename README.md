**Feladat: **
Egy kétdimenziós pályán egy bábut mozgathat a felhasználó. 

**A játék célja:**
Minden ajándékot összegyűjteni. Ha bombára lépünk, vége a játéknak. Minden lépés után írjuk ki, hány ajándék maradt a pályán.

A pálya méretét a játékos adja meg. 
A sorok és oszlopok száma legalább tíz, legfeljebb húsz. 

A pályán véletlenszerűen generálunk húsz bombát úgy, hogy egy mezőn maximum csak egy bomba lehet. 
Ezek mellett generálunk húsz ajándékot is. 
Az ajándékokra is igaz, egy mezőn legfeljebb egy ajándék lehet. 
Ha olyan helyre generálunk ajándékot, ahol bomba van, akkor semlegesítik egymást (azon a mezőn nincs sem ajándék, sem bomba). 

**Mozgás:**
A “j” karakterrel egyet jobbra, a “b” karakterrel egyet balra, az “f” karakterrel egyet fel, míg az “l” karakterrel egyet le tudunk lépni. 
