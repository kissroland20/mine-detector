/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gamehf;

import java.util.Scanner;

/**
 * Feladat: Egy kétdimenziós pályán egy bábut mozgathat a
 * felhasználó. A pálya méretét a játékos adja meg. A sorok és oszlopok száma
 * legalább tíz, legfeljebb húsz. A pályán véletlenszerűen generálunk húsz
 * bombát úgy, hogy egy mezőn maximum csak egy bomba lehet. Ezek mellett
 * generálunk húsz ajándékot is. Az ajándékokra is igaz, egy mezőn legfeljebb
 * egy ajándék lehet. Ha olyan helyre generálunk ajándékot, ahol bomba van,
 * akkor semlegesítik egymást (azon a mezőn nincs sem ajándék, sem bomba). A “j”
 * karakterrel egyet jobbra, a “b” karakterrel egyet balra, az “f” karakterrel
 * egyet fel, míg az “l” karakterrel egyet le tudunk lépni. A cél, hogy minden
 * ajándékot összeszedjünk. Ha bombára lépünk, vége a játéknak. Minden lépés
 * után írjuk ki, hány ajándék maradt a pályán.
 * Mozogni a "j" (jobbra), "b" (balra) , "f" (fel) és "l" (le) betűkkel lehet.
 *
 * @author rolan
 */
public class GameHF {
    public static final Scanner SCANNER = new Scanner(System.in);

    public static final int DEFAULT_MATRIX = 10;
    public static final int NUMBERS_OF_BOMBS = 20;
    public static final int EMPTY = 0;
    public static final int BOMB = 8;
    public static final int PLAYER = 1;
    public static final int GIFT = 5;
    public static final int NUMBER_OF_GIFTS = 20;
    
    public static final String UP = "f";
    public static final String DOWN = "l";
    public static final String LEFT ="'b";
    public static final String RIGHT = "j";
    
    public static final String MESSAGE_OF_END_OF_THE_GAME = "Bombára léptél! Vége a játéknak!";
    public static final String MESSAGE_OF_A_GIFT = "Így tovább! Ez egy ajánék volt!";
    public static final String MESSAGE_OF_NUMBER_OF_GIFTS_BACK = "Ennyi ajándék maradt: ";
    public static final String WINNING_MESSAGE = "Gratulálunk! GYŐZTÉL!!!";
    public static final String TYPE_AN_EVEN_NUMBER_MESSAGE = "Kérlek adj meg egy egész számot!!";
    
    public static final int[][] MATRIX = createField();

    public static int xyz() {
        
        while(!SCANNER.hasNextInt()) {
            
            System.out.println(TYPE_AN_EVEN_NUMBER_MESSAGE);
        }
        return SCANNER.nextInt();
    }
    
    public static int[][] createField() {

        int num1 = xyz();
        int num2 = xyz();

        if (num1 < 10 || num2 < 10 || num1 > 20 || num2 > 20) {

            return new int[DEFAULT_MATRIX][DEFAULT_MATRIX];

        }
        return new int[num1][num2];
    }

    public static void printField(int[][] array) {

        System.out.println();

        for (int i = 0; i < array.length; i++) {

            for (int j = 0; j < array[i].length; j++) {

                System.out.print(array[i][j]);

            }
            System.out.println();

        }

    }

    public static int randomNumberGenerator(int from, int to) {

        if (to > from) {

            return (int) (Math.random() * (to - from) + from);
        }

        return (int) (Math.random() * (from - to) + to);
    }

    public static void setBombs(int[][] array) {

        for (int i = 0; i < NUMBERS_OF_BOMBS; i++) {

            int num1 = randomNumberGenerator(0, array.length);
            int num2 = randomNumberGenerator(0, array[num1].length);

            if (array[num1][num2] == EMPTY) {

                array[num1][num2] = BOMB;
            } else {
                i--;
            }
        }
    }

    public static void setElement(int[][] space, int count, int element, int neutral) {
        
      
        for (int i = 0; i < count; i++) {

            int num1 = randomNumberGenerator(0, space.length);
            int num2 = randomNumberGenerator(0, space[num1].length);

            if(space[num1][num2] == element) {
                
                space[num1][num2] = element;
            } else if (space[num1][num2] == neutral) {
                space[num1][num2] = EMPTY;
            } else {
                i--;
            }
            
            
//            switch (space[num1][num2]) {
//                
//                case localElement: space[num1][num2] = GIFT;
//                    break;
//                case localNeutral: space[num1][num2] = EMPTY;
//                    break;
//                default: i--;
//            }
     
        }
    }
    
    
    
    public static void setGifts(int[][] array) {

        for (int i = 0; i < NUMBER_OF_GIFTS; i++) {

            int num1 = randomNumberGenerator(0, array.length);
            int num2 = randomNumberGenerator(0, array[num1].length);

            switch (array[num1][num2]) {
                
                case EMPTY: array[num1][num2] = GIFT;
                    break;
                case BOMB: array[num1][num2] = EMPTY;
                    break;
                default: i--;
            }
     
        }
    }

    public static void setPlayer(int[][] array) {

        array[0][0] = PLAYER;
    }

    
    /*
    A “j” karakterrel egyet jobbra, 
    a “b” karakterrel egyet balra, 
    az “f” karakterrel egyet fel, 
    míg az “l” karakterrel egyet le tudunk lépni.
    */
    
    public static void play(int[][] array) {
        
        int numberOfBombs = howManyBombsExist(array);
        int numberOfGifts = howManyGiftsStillExist(array);
        
        
      for(int i = 0; 0 != howManyGiftsStillExist(array);i++){
          
        switchMethod(array);

          if (numberOfBombs != howManyBombsExist(array)) {
              System.out.println(MESSAGE_OF_END_OF_THE_GAME);
              return;
          } else if (howManyGiftsStillExist(array) == 0) {
              System.out.println(WINNING_MESSAGE);
              return;
          }
          printField(array);
      }
      
        
    }
    
    public static void switchMethod(int[][] array) {
        
          switch (SCANNER.next()) {
              case RIGHT: 
                  right(array);
              break;
              case LEFT: 
                  left(array);
              break;
              case UP: 
                  up(array);
              break;
              case DOWN: 
                  down(array);
              break;
          }
    }
    
    
     public static int howManyBombsExist(int[][] array) {
        
        int count = 0;
        
         for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                
                if(array[i][j] == BOMB) {
                    count++;
                }
            }
            }
         return count;
    }
      
    public static int howManyGiftsStillExist(int[][] array) {
        
        int count = 0;
        
         for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                
                if(array[i][j] == GIFT) {
                    count++;
                }
            }
            }
         return count;
    }
    
    
    public static int[][] right(int[][] array) {
                
      
       for (int i = 0; i < array.length; i++) {
        
           for (int j = 0; j < array[i].length - 1; j++) {
                             
                if (array[i][j] == PLAYER) {

                    switch (array[i][j + 1]) {
                        case BOMB:
                            
                            array[i][j + 1] = EMPTY;
                            return array;
                        case GIFT:
                            System.out.println(MESSAGE_OF_A_GIFT);

                            break;
                        }
                    array[i][j] = EMPTY;
                    array[i][j + 1] = PLAYER;
                    System.out.println(MESSAGE_OF_NUMBER_OF_GIFTS_BACK + howManyGiftsStillExist(array));
                    return array;
                }
         }
        }
      
             return array;   
    }
    
    public static int[][] left(int[][] array) {
                
        for (int i = 0;i < array.length;i++) {
          for (int j = 0;j < array[i].length;j++)  {
                   
             if (array[i][j] == PLAYER) {
             
             switch (array[i][j-1]) {
                 case BOMB:
                     System.out.println(MESSAGE_OF_END_OF_THE_GAME);
                 return array;
                 case GIFT:
                     System.out.println(MESSAGE_OF_A_GIFT);
                     break;
               
             }
             array[i][j] = EMPTY;
                    array[i][j-1] = PLAYER;
                    System.out.println(MESSAGE_OF_NUMBER_OF_GIFTS_BACK + howManyGiftsStillExist(array));
                    return array;
             }
             }         
             }
      
             return array;   
    }
    
    public static int[][] up(int[][] array) {
       
        for (int i = 0;i < array.length;i++) {
          for (int j = 0;j < array[i].length;j++)  {
          
         
             if (array[i][j] == PLAYER) {
             
             switch (array[i-1][j]) {
                 case BOMB:
                     System.out.println(MESSAGE_OF_END_OF_THE_GAME);
                 return array;
                 case GIFT:
                     System.out.println(MESSAGE_OF_A_GIFT);
                     
                 break;
                 
             }
             array[i][j] = EMPTY;
                    array[i-1][j] = PLAYER;
                    System.out.println(MESSAGE_OF_NUMBER_OF_GIFTS_BACK + howManyGiftsStillExist(array));
                    return array;
             }

              }         
             }
      
             return array;   
    }
    
    public static int[][] down(int[][] array) {
        
          for (int i = 0;i < array.length-1;i++) {
          for (int j = 0;j < array[i].length;j++)  {
          
         
             if (array[i][j] == PLAYER) {
             
             switch (array[i+1][j]) {
                 case BOMB:
                     System.out.println(MESSAGE_OF_END_OF_THE_GAME);
                 return array;
                 case GIFT:
                     System.out.println(MESSAGE_OF_A_GIFT);
                     
                 break;
             }
             array[i][j] = EMPTY;
                    array[i+1][j] = PLAYER;
                    System.out.println(MESSAGE_OF_NUMBER_OF_GIFTS_BACK + howManyGiftsStillExist(array));
                    return array;
             }

              }         
             }
      
             return array;   
    }

    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        System.out.println("JÁTÉKSZABÁLYOK:");
        System.out.println("Ahhoz, hogy nyerj, gyűjtsd össze az összes ajándékot(amit az 5 szám jelöl)!");
        System.out.println("Ha bombára lépsz (ami a 8-as szám), vége a játéknak!");
        System.out.println("Add meg mekkora pályán akarsz játszani (10 és 20 közötti számok):");

        int[][] matrix = createField();
        setBombs(matrix);
        setGifts(matrix);
        setPlayer(matrix);
        printField(matrix);
        play(matrix);
        SCANNER.close();
    }
}
